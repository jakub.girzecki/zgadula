import random

menuList = ["1. Start New Game", "2. Options", "0. Exit"]


def newGame():
    number = random.randint(0, 100)
    start = True
    while start:
        print("podaj liczbe z przedzialu 0 - 100")
        yourNamber = int(input())
        if yourNamber == number:
             print("Zgadles")
             start = False
        elif yourNamber < number:
             print("twoja liczba jest mniejsza")
        else:
             print("twoja liczba jest większa")


def mainMenu():
    start = True
    while start:
        for item in menuList:
            print(item)
        wybor = int(input())
        if wybor == 1:
            newGame()
        elif wybor == 2:
            print("Options")
        elif wybor == 0:
            print("Wyjscie")
            start = False
        else:
            print("podaj element z menu")


if __name__ == "__main__":
    mainMenu()